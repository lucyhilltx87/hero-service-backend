package com.lucyhill.heroStats.repositories;

import com.lucyhill.heroStats.interfaces.HeroRepository;
import com.lucyhill.heroStats.objects.Hero;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TestRepository implements HeroRepository {
    List<Hero> heroes = new ArrayList<>();
    @Override
    public List<Hero> findAll() {
        return heroes;
    }

    @Override
    public List<Hero> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Hero> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Hero> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return heroes.size();
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Hero hero) {

    }

    @Override
    public void deleteAll(Iterable<? extends Hero> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Hero> S save(S s) {
        heroes.add(s);
        return s;
    }

    @Override
    public <S extends Hero> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<Hero> findById(Long aLong) {
        for(Hero hero : heroes) {
            if(hero.getId() == aLong) {
                return Optional.of(hero);
            }
        }
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Hero> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Hero> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Hero getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends Hero> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Hero> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Hero> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Hero> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Hero> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Hero> boolean exists(Example<S> example) {
        return false;
    }
}
