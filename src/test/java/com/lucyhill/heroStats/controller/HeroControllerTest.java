package com.lucyhill.heroStats.controller;

import com.lucyhill.heroStats.interfaces.HeroRepository;
import com.lucyhill.heroStats.objects.Hero;
import com.lucyhill.heroStats.repositories.TestRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class HeroControllerTest {

    private HeroRepository repository;
    private Hero hero;
    private HeroController heroController;
    @Before
    public void setup() {
        hero = new Hero();
        hero.setId(1L);
        hero.setName("Test Hero");

        repository = new TestRepository();

        heroController = new HeroController(repository);
    }

    @Test
    public void saveHeroTestDidSave() {
        heroController.addHeroToRepository(hero);
        Optional<Hero> foundHero = repository.findById(1L);
        assertTrue(foundHero.isPresent());
    }

    @Test
    public void saveHeroTestDidSaveCorrectly() {
        heroController.addHeroToRepository(hero);
        Optional<Hero> foundHero = repository.findById(1L);
        Hero savedHero = foundHero.get();
        assertEquals(hero.getName(), savedHero.getName());
    }

    @Test
    public void getAllHeroesDoReturnOneHero() {
        repository.save(hero);
        List<Hero> heroes = heroController.getAllHeroes();
        assertEquals(1, heroes.size());
    }

    @Test
    public void getAllHeroesDoesReturnCorrectHero() {
        repository.save(hero);
        List<Hero> heroes = heroController.getAllHeroes();
        assertEquals(hero.getName(), heroes.get(0).getName());
    }

    @Test
    public void getAllHeroesReturnMoreThanOneHero() {
        repository.save(hero);
        repository.save(hero);
        repository.save(hero);
        List<Hero> heroes = heroController.getAllHeroes();
        assertEquals(3, heroes.size());
    }

}