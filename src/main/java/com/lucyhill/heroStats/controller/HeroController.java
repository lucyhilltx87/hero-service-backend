package com.lucyhill.heroStats.controller;

import com.lucyhill.heroStats.interfaces.HeroRepository;
import com.lucyhill.heroStats.objects.Hero;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
public class HeroController {

    private HeroRepository repository;

    public HeroController(HeroRepository heroRepository) {
        this.repository = heroRepository;
    }

    @PostMapping("/heroes")
    public void addHeroToRepository(@RequestBody Hero hero) {
        repository.save(hero);
    }

    @GetMapping("/heroes")
    public List<Hero> getAllHeroes() {
        return repository.findAll();
    }
}
