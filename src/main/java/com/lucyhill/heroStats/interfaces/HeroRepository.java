package com.lucyhill.heroStats.interfaces;

import com.lucyhill.heroStats.objects.Hero;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HeroRepository extends JpaRepository<Hero, Long> {}