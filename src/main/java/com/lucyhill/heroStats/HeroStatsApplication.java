package com.lucyhill.heroStats;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HeroStatsApplication {

	public static void main(String[] args) {
		SpringApplication.run(HeroStatsApplication.class, args);
	}

}
